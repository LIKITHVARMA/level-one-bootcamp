#include<stdio.h>
#include<math.h>
typedef struct point
{
  float x;
  float y;
} Point;
Point input()
{
    Point p;
    printf("\nEnter abscissa:");
    scanf("%f",&p.x);
    printf("Enter ordinate:");
    scanf("%f",&p.y);
    return p;
}
float distance_between_two_points(Point p1, Point p2)
{
   float distance;
   distance=sqrt(((p2.x-p1.x)*(p2.x-p1.x))+((p2.y-p1.y)*(p2.y-p1.y)));
   return distance;
}
void result(float dist)
{
   printf("The distance between the two points is %.2f",dist);
}
int main()
{
    Point p1,p2;
    float dist;
    printf("Enter the coordinates of point X");
    p1=input();
    printf("Enter the coordinates of point Y");
    p2=input();
    dist=distance_between_two_points(p1,p2);
    result(dist);
    return 0;
}
